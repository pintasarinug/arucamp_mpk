﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GlobalScript : MonoBehaviour {
    // Start is called before the first frame update
    private float complete;
    private float totalMerah= 6f;
    public Text hasil;
    static public GlobalScript Instance;

    void Start () {
        Instance = this;
        complete = totalMerah / 6 * 100;
        hasil.text = " Completeness " + complete + "%";
    }

    // Update is called once per frame
    void Update () {

    }
    public void addComplete (bool b) {
        if (b) {
            totalMerah += 1f;
        } else {
            totalMerah -= 1f;
        }

        complete = (totalMerah / 6f )* 100f;
        hasil.text = " Completeness " + complete + "%";
    }
}