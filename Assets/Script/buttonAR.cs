﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using UnityEngine.SceneManagement ;

public class buttonAR : MonoBehaviour {

    public GameObject apd, buttonMerah, buttonHijau;

    public void toggleApd () {
        apd.SetActive (!apd.activeSelf);
        buttonHijau.SetActive (!buttonHijau.activeSelf);
        buttonMerah.SetActive (!buttonMerah.activeSelf);
        GlobalScript.Instance.addComplete (apd.activeSelf);
    }

    // Start is called before the first frame update
    void Start () {

    }

    // Update is called once per frame
    void Update () {

    }
}