﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
public class Play_Vidio : MonoBehaviour {
    // Start is called before the first frame update
    public VideoPlayer videoPlayer;
    public GameObject btn_pause, btn_play;
    void Start () {

        //videoPlayer.Play ();
    }

    public void playVideo () {
            btn_pause.SetActive(!btn_pause.activeSelf);
            btn_play.SetActive(!btn_play.activeSelf);
        if (videoPlayer.isPlaying) {
            videoPlayer.Pause ();
        } else {
            videoPlayer.Play ();
        }
    }

}